#!/bin/sh

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=${script_dir}/../etc
styles_dir=${script_dir}/../styles
style_tex=${styles_dir}/cv.tex
style_css=${styles_dir}/cv.css
src_basename=sandra-morris
src_filename=${src_basename}.md

substitute() {
 sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
     -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
     -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
     -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
     -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
     -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
     -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
     -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
     -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
     ${src_dir}/${src_filename} > ${dist_dir}/${src_filename}
}

buildPdf() {
  pandoc --standalone --template ${style_tex} --from markdown --to context --variable papersize=A4 --output ${dist_dir}/${src_basename}.tex ${dist_dir}/${src_filename}
  mtxrun --path=${dist_dir} --result=${src_basename}.pdf --script context ${src_basename}.tex
}

buildHtml() {
  pandoc --standalone --include-in-header ${style_css} --from markdown --to html --output ${dist_dir}/${src_basename}.html ${dist_dir}/${src_filename} --metadata pagetitle=${src_basename}
}

buildDocx() {
  pandoc --standalone ${dist_dir}/${src_filename} --output ${dist_dir}/${src_basename}.docx
}

buildRtf() {
  pandoc --standalone ${dist_dir}/${src_filename} --output ${dist_dir}/${src_basename}.rtf
}

buildAllFormats() {
  buildPdf
  buildHtml
  buildDocx
  buildRtf
}

mkdir -p ${dist_dir}
substitute
buildAllFormats
rsync -aH ${etc_dir}/ ${dist_dir}
