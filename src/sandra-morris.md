Sandra Morris
=============

----

> <sandra@tmorris.net> • 0403596618 •
> Gold Coast, Australia

----

Professional Experience
-----------------------

2015-*present*
:   **Administrator**
:   B & P Surveys

<div/>

2023-2023
:   **Massage Therapist**
:   City Cave Yarrabilba

<div/>

2022-2023
:   **Youth Worker / Residential Care**
:   Uniting Care

<div/>

2013-2015
:   **Receptionist**
:   TKM Accountants

<div/>

2010-2013
:   **Administrator / Customer Service**
:   Noosa Screens and Curtains

<div/>

2009-2010
:   **Housekeeping**
:   Australis Noosa Lakes Resort

<div/>

2008-2015
:   **Cleaner**
:   Jim's Cleaning

<div/>

2007-2009
:   **Administrator and Residential Care**
:   Youth Focus / United Synergies

<div/>

2007-2009
:   **Reception, Showroom & Accounts**
:   Solomons Security and Blinds

<div/>

2006-2007
:   **Front Reception & Accounts**
:   Graham Lukins Partnership

<div/>

2006
:   **Data Entry & Telesales**
:   Australian Discount Holidays

<div/>

2004-2006
:   **Reception, Showroom & Accounts**
:   Special Equipment Australasia

<div/>

2002-2004
:   **Administrator and Residential Care**
:   Youth Focus / United Synergies

<div/>

2002
:   **Cashier**
:   Pickles Auctions

Voluntary Work Experience
-------------------------

2009
:   **Receptionist**
:   Suncoast Community Legal Services Inc

<div/>

2007-2009
:   **Treasurer**
:   Caloundra Community Centre

<div/>

2007-2008
:   **Telephone Counsellor**
:   Lifeline

<div/>

2002
:   **Customer Service**
:   Wynnum Manly Information Centre

<div/>

1999
:   **Receptionist**
:   Quest Newspaper Wynnum Office

Responsibilities
----------------

* Front Reception including Customer Service and Showroom
* General office duties including correspondence, ordering, filing, archiving, cleaning, coffee making
* Payroll, Accounts Payable & Receivable, Bank Reconciliation using Xero/MYOB
* Booking of flights & Accommodation for staff
* Appointment/diary booking for sales reps and other staff
* Costing and typing quotations
* Applications for ABN, TFN, Company’s, Trusts and SMSFs
* ASIC, CAS and NTAA
* Reporting to Government Agencies, Grant Submissions and Statistics
* Tenancy, Residential Care and working with young people

Education
---------

2016-*present*
:   Justice of the Peace (Qual)

<div/>

2022
:   Diploma of Remedial Massage _(DipRM)_, Evolve College

<div/>

2016-2022
:   Bachelor of Counselling _(BCouns)_, Australian Colleg of Applied Psychology (ACAP)

References
----------

Greg Gossip
:   B & P Surveys 0409391906

<div/>

Peter Turner
:   TKM Accountants 0754440544

<div/>

Mandy Botha
:   Noosa Screens & Curtains 0754497722

----

> <sandra@tmorris.net> • 0403596618 •
> Gold Coast, Australia

<div style="color:#808080; font-style:italic; font-size:0.4em; text-align:right">
  Last updated: <b>${COMMIT_TIME}</b>
</div>
